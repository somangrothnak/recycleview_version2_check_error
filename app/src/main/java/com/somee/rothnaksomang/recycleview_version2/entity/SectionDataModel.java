package com.somee.rothnaksomang.recycleview_version2.entity;

import java.util.ArrayList;

public class SectionDataModel {
    private String headerTitle;
    private ArrayList<SingleItemModel> allItemInSection;

    public SectionDataModel(String headerTitle, ArrayList<SingleItemModel> allItemInSection) {
        this.headerTitle = headerTitle;
        this.allItemInSection = allItemInSection;
    }
    public SectionDataModel(){}


    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public ArrayList<SingleItemModel> getAllItemInSection() {
        return allItemInSection;
    }

    public void setAllItemInSection(ArrayList<SingleItemModel> allItemInSection) {
        this.allItemInSection = allItemInSection;
    }

    @Override
    public String toString() {
        return "SectionDataModel{" +
                "headerTitle='" + headerTitle + '\'' +
                ", allItemInSection=" + allItemInSection +
                '}';
    }
}
