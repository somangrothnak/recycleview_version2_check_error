package com.somee.rothnaksomang.recycleview_version2.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.somee.rothnaksomang.recycleview_version2.R;
import com.somee.rothnaksomang.recycleview_version2.entity.SingleItemModel;

import java.util.ArrayList;

public class SectionListDataAdapter extends RecyclerView.Adapter<SectionListDataAdapter.SingleItemRowHolder>{
    private ArrayList<SingleItemModel> itemModels;
    private Context context;

    public SectionListDataAdapter(ArrayList<SingleItemModel> itemModels, Context context) {
        this.itemModels = itemModels;
        this.context = context;
    }


    @NonNull
    @Override
    public SingleItemRowHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_adapter,null);
        SingleItemRowHolder singleItemRowHolder=new SingleItemRowHolder(view);
        return  singleItemRowHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SingleItemRowHolder singleItemRowHolder, int i) {
            SingleItemModel itemModel=itemModels.get(i);
            singleItemRowHolder.tvTitle.setText(itemModel.getName());
    }

    @Override
    public int getItemCount() {
        return itemModels.size();
    }

    class SingleItemRowHolder extends RecyclerView.ViewHolder{

        protected TextView tvTitle;
        protected ImageView ivImage;

        public SingleItemRowHolder(@NonNull View itemView) {
            super(itemView);
            this.tvTitle=itemView.findViewById(R.id.tv_title);
            this.ivImage=itemView.findViewById(R.id.iv_image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(),tvTitle.getText(),Toast.LENGTH_SHORT).show();
                }
            });

        }
    }
}
