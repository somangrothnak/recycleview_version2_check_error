package com.somee.rothnaksomang.recycleview_version2.entity;

public class SingleItemModel {
    private String name,url,description;

    public SingleItemModel(String name, String url, String description) {
        this.name = name;
        this.url = url;
        this.description = description;
    }
    public SingleItemModel(String name, String ur) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "SingleItemModel{" +
                "name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
