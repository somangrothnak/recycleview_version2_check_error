package com.somee.rothnaksomang.recycleview_version2.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.somee.rothnaksomang.recycleview_version2.R;
import com.somee.rothnaksomang.recycleview_version2.entity.SectionDataModel;

import java.util.ArrayList;

public class RecycleViewDataAdapter extends RecyclerView.Adapter<RecycleViewDataAdapter.ItemRowHolder>{

    private ArrayList<SectionDataModel> dataList;
    private Context context;
    private RecyclerView.RecycledViewPool recycledViewPool;

    public RecycleViewDataAdapter(ArrayList<SectionDataModel> dataList, Context context) {
        this.dataList = dataList;
        this.context = context;
        recycledViewPool = new RecyclerView.RecycledViewPool();
    }

    @NonNull
    @Override
    public ItemRowHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_adapter_horizontal_scroll,null);
        ItemRowHolder itemRowHolder=new ItemRowHolder(view);
        return itemRowHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemRowHolder itemRowHolder, int i) {
        final String sectionName=dataList.get(i).getHeaderTitle();
        ArrayList singleSectionItems=dataList.get(i).getAllItemInSection();
//        Log.e("DataRecycler","Data=>"+singleSectionItems.toString());

        itemRowHolder.tv_title.setText(sectionName.toString());

        SectionListDataAdapter adapter=new SectionListDataAdapter(singleSectionItems,context);
        itemRowHolder.recyclerView.setHasFixedSize(true);
        itemRowHolder.recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
        itemRowHolder.recyclerView.setRecycledViewPool(recycledViewPool);
        itemRowHolder.recyclerView.setAdapter(adapter);
        itemRowHolder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(),"Button More Clicked!"+sectionName,Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class ItemRowHolder extends RecyclerView.ViewHolder{

        protected TextView tv_title;
        protected RecyclerView recyclerView;
        protected Button btnMore;

        public ItemRowHolder(@NonNull View itemView) {
            super(itemView);
            tv_title=itemView.findViewById(R.id.tv_main_title);
            recyclerView=itemView.findViewById(R.id.rv_allitem);
            btnMore=itemView.findViewById(R.id.btn_more);
        }
    }
}
